import React from "react"
import {widgets} from "react-ioc-widgets"
import {FormGroup, Input, Label} from "reactstrap"

function GeneralDetails({doUpdate, focus: {selectedQuestion}}) {
    return <>
        <FormGroup>
            <Label>Caption</Label>
            <Input value={selectedQuestion.caption} onChange={e=>{
                selectedQuestion.caption = e.target.value
                doUpdate()
            }}/>
        </FormGroup>
        <FormGroup>
            <Label>Model</Label>
            <Input value={selectedQuestion.model} onChange={e=>{
                selectedQuestion.model = e.target.value
                doUpdate()
            }}/>
        </FormGroup>
    </>
}

widgets.on("edit.survey", function ({focus, editor}) {
    if (focus.selectedQuestion) {
        editor.tabs.general.content.push(GeneralDetails)
    }
})
