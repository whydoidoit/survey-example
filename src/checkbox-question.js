import React, {useState} from "react"
import {widgets} from "react-ioc-widgets"
import {FormGroup, Input, Label} from "reactstrap"

function CheckboxQuestion({design, value, setValue}) {
    const [localValue, setLocalValue] = useState(value)
    return <FormGroup className="checkbox">
        <Label>&nbsp;&nbsp;<Input type="checkbox" checked={localValue === design.checkedValue} onChange={e => {
            let value = e.target.checked ? design.checkedValue : design.uncheckedValue
            setLocalValue(value)
            setValue(value)
        }}/>{design.caption}</Label>

    </FormGroup>
}
CheckboxQuestion.priority = 2
CheckboxQuestion.callBeforeRender = (contents) => {
    contents.length = 0
    contents.push(CheckboxQuestion)
}


widgets.on("editor.question.list", function ({content, design}) {
    if(design.options && design.options.length == 2 && design.options.some(option=>option.value === "y")) {
        design.checkedValue = "y"
        design.uncheckedValue = design.options.find(option=>option.value !== "y").value
        content.push(CheckboxQuestion)
    }
})
