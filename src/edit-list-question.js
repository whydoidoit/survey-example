import React from "react"
import {widgets} from "react-ioc-widgets"
import {Button, Col, FormGroup, Input, Label, Row} from "reactstrap"

function ListOfOptions({doUpdate, focus: {selectedQuestion}}) {
    return <> {selectedQuestion.options.map((option, index) => {
        return <Row key={index}>
            <Col>
                <FormGroup>
                    <Label>Value</Label>
                    <Input value={option.value} onChange={e => {
                        option.value = e.target.value
                        doUpdate()
                    }}/>
                </FormGroup>
            </Col>
            <Col>
                <FormGroup>
                    <Label>Value</Label>
                    <Input value={option.text} onChange={e => {
                        option.text = e.target.value
                        doUpdate()
                    }}/>
                </FormGroup>
            </Col>
            <Col sm="auto">
                <Button onClick={()=>{
                    selectedQuestion.options.splice(selectedQuestion.options.indexOf(option), 1)
                    doUpdate()
                }}>Remove</Button>
            </Col>
        </Row>
    })}
        <Button onClick={()=>{
            selectedQuestion.options.push({text: "New option", value: ""})
            doUpdate()
        }}>Add Option</Button>
    </>
}

widgets.on("edit.survey", function ({focus, editor}) {
    if (focus.selectedQuestion && Array.isArray(focus.selectedQuestion.options)) {
        editor.tabs.options = editor.tabs.options || {
            title: "Options",
            content: [ListOfOptions]
        }
    }
})
