import React from "react"
import {widgets} from "react-ioc-widgets"
import {Button} from "reactstrap"

function DeleteButton({design, doUpdate}) {
    return <Button onClick={()=>{
        debugger
        design.list.splice(design.list.indexOf(design.item), 1)
        doUpdate()
    }} color="danger">X</Button>
}

widgets.on("inline.list", function ({endOfLine}) {
    endOfLine.push(DeleteButton)
})
