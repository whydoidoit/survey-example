import React, {useState} from "react"
import {widgets} from "react-ioc-widgets"
import {FormGroup, Input, Label} from "reactstrap"

function ListQuestion({design, value, setValue}) {
    const [localValue, setLocalValue] = useState(value)
    return <FormGroup>
        <Label>{design.item.caption}</Label>
        <Input type="select" value={localValue} onChange={e => {
            setLocalValue(e.target.value)
            setValue(e.target.value)
        }}>
            {design.item.options.map(option => {
                return <option key={option.value} value={option.value}>
                    {option.text}
                </option>
            })}
        </Input>
    </FormGroup>
}

widgets.on("editor.question.list", function ({content}) {
    content.push(ListQuestion)
})

widgets.on("survey.questiontypes", function (types) {
    types.push({
        color: "warning", caption: "List", create() {
            return {
                type: "list",
                options: [],
                caption: "New List",
                model: "newModel"
            }
        }
    })
})
