import React from "react"
import {arrayMoveInPlace, SortableWidget, SortableWidgetContainer, widgets} from "react-ioc-widgets"
import injectSheet from "react-jss"

let styles = {
    heading: {
        textAlign: "center",
        fontSize: "larger",
        fontWeight: "bold"
    }
}

const SurveyTitle = injectSheet(styles)(function SurveyTitle({design, classes}) {
    return <>
        <div className={classes.heading}>{design.name}</div>
    </>
})

const Questions = injectSheet(styles)(function Questions({questions, answers, changed, focus, doUpdate}) {
    return <SortableWidgetContainer design={questions} focusOn={{selectedQuestions: questions}}
                                    distance={4}
                                    onSortEnd={({oldIndex, newIndex})=>{
                                        arrayMoveInPlace(questions, oldIndex, newIndex)
                                        doUpdate()
                                    }}
                                    isSelected={focus.selectedQuestions === questions} type="questions">
        {questions.map((question, index) => {
            return <SortableWidget
                index={index}
                focusOn={{selectedQuestions: questions, selectedQuestion: question}}
                key={question.model}
                type={[`question.${question.type}`, "list"]}
                value={answers[question.model] || ""}
                setValue={(value) => {
                    answers[question.model] = value
                    changed()
                }}
                design={{item: question, list: questions}}>

            </SortableWidget>
        })}
    </SortableWidgetContainer>
})


widgets.on("edit.survey", function ({layout}) {
    layout.headerCentre.push(SurveyTitle)
    layout.content.push(({design, document, ...props}) => <Questions {...props} questions={design.questions}
                                                                     answers={document.answers}/>)
})
