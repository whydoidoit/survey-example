import React, {useState} from "react"
import {widgets} from "react-ioc-widgets"
import {FormGroup, Input, Label} from "reactstrap"

function TextQuestion({design, value, setValue}) {
    const [localValue, setLocalValue] = useState(value)
    return <FormGroup>
        <Label>{design.item.caption}</Label>
        <Input value={localValue} onChange={e => {
            setLocalValue(e.target.value)
            setValue(e.target.value)
        }}/>
    </FormGroup>
}


widgets.on("editor.question.text", function ({content}) {
    content.push(TextQuestion)
})

widgets.on("survey.questiontypes", function (types) {
    types.push({
        color: "primary", caption: "Text", create() {
            return {
                type: "text",
                caption: "New text field",
                model: "newModel"
            }
        }
    })
})
