import React from "react"
import {widgets} from "react-ioc-widgets"
import {Button, FormGroup, Input, Label} from "reactstrap"
import injectSheet from "react-jss"

function EditSurveyTitle({design, doUpdate}) {
    return <FormGroup>
        <Label>Document Title</Label>
        <Input value={design.name || ""} onChange={(e) => {
            design.name = e.target.value
            doUpdate()
        }}/>
    </FormGroup>
}

function AddQuestions({design, doUpdate}) {
    const availableTypes = []
    widgets.emit("survey.questiontypes", availableTypes)
    return availableTypes.length === 0 ? null : availableTypes.map(type => {
        return <Button onClick={() => {
            let newQuestion = type.create()
            design.push(newQuestion)
            doUpdate()
        }} key={type.caption} color={type.color || "success"}>{type.caption}</Button>
    })
}

widgets.on("inline.questions", function ({inline, endOfLine}) {
    inline.push(AddQuestions)
})

widgets.on("edit.survey", function ({editor}) {
    editor.tabs.general.content.push(EditSurveyTitle)
})
