import React, {Component} from 'react'
import './App.css'
import {Widgets} from "react-ioc-widgets"
import "./survey"
import "bootstrap/dist/css/bootstrap.min.css"
import "./text-question"
import "./list-question"
import "./checkbox-question"
import "./edit-survey"
import "./edit-general-question"
import "./edit-list-question"
import "./delete-from-list"

import design from "./questions"

let document = { answers: {} }

class App extends Component {
    render() {
        return (
            <div className="App">
                <Widgets style={{height: "calc(100vh - 1em)"}} design={design} document={document} editable={true} type="survey" updated={()=>{
                    console.log(document)
                }}/>
            </div>
        )
    }
}

export default App
